declare interface DataBar {
    labels: string[];
    datasets: DataSet[];
}

declare interface DataSet {
    label: string;
    data: number[];
    backgroundColor: string[];
    borderWidth: number;
    borderColor: string[];
}

declare interface MarketData {
    pair: string;
    markets: string[];
}

declare interface PriceData {
    pair: string;
    markets: string[];
    prices: number[];
}

declare type DataEntry = {
    id: string;
    label: string;
    value: number;
    color: string;
}

declare interface SelectOption {
    value: string;
    label: string;
}

declare interface AuthOpts {
    isOpen: boolean;
    toggleAuthModal: function;
    logIn: function;
}

declare interface DifValue {
    value: number;
    label: number;
    sid: number;
}

declare interface AnySignal {
    id: number;
    pair: string;
    markets: string[];
    difference: number;
}

declare interface SpecificSignal {
    sid: number;
    value: number;
    label: number;
}

declare interface MarketPairs {
    market: string;
    pairs: string[];
}

declare interface PairName {
    source: string[];
    destination: string;
}

declare interface StepperStep {
    id: number,
    title: string
}