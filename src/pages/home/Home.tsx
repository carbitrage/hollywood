import React, { Component } from "react";
import { MDBContainer, MDBCard, MDBCardHeader, MDBCardBody, MDBCardText, MDBCardTitle } from 'mdbreact';
import Stepper, { Step } from "react-material-stepper";
import 'react-material-stepper/dist/react-stepper.css';
import './Home.css';

class Home extends Component<HomeProps> {
  componentDidMount() {
    this.props.setActiveTab('Home');
  }

  render() {
    const steps: StepperStep[] = [
      { id: 1, title: 'Log In' },
      { id: 2, title: 'Select Data' },
      { id: 3, title: 'Create Signals' },
      { id: 4, title: 'Receive Signals' }
    ];

    return(
      <MDBContainer>
        <MDBCard border="light" className="m-3">
          <MDBCardHeader>Welcome</MDBCardHeader>
          <MDBCardBody>
            <MDBCardTitle>Cryptocurrency Arbitrage</MDBCardTitle>
            <MDBCardText>
              Welcome to Carbi - service that allows crypto investors to earn interest on price difference for cryptocurrencies 
              between cryptomarkets.
              <br></br>You'll need Telegram to contact our bot and receive signals.
              <br></br>Mobile app is coming soon.
            </MDBCardText>
            <MDBCardText>
              Check out steps below to start with cryptycurrency arbitrage for free.
            </MDBCardText>
            <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-around', marginTop: '10px' }}>
              <Stepper vertical={ this.props.width < 768 ? true : false } initialStep={ this.props.loggedIn ? 3 : 1 }>
                { 
                  steps.map((step: StepperStep) =>
                    <Step
                      stepId={ step.id }
                      title={ step.title }
                    >
                      <div/>
                    </Step>
                  )
                }
              </Stepper>
            </div>
          </MDBCardBody>
        </MDBCard>
      </MDBContainer>
    );
  }
}

export default Home;