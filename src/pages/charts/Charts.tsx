import React, { Component } from "react";
import Cookies from 'universal-cookie';
import axios, { AxiosResponse, AxiosError } from 'axios';
import { ValueType } from "react-select";
import { toast } from 'react-toastify';
import Chart from '../../components/chart/Chart';
import Options from "../../components/options/Options";
import Signals from "../../components/signals/Signals";
import 'dotenv';
import pairsAndMarkets from '../../data/markets.json';
import pairsNames from '../../data/pairs.json';

class Charts extends Component<ChartsProps> {
  state: ChartsState = {
    pair: [...pairsNames][0].destination,
    markets: [...pairsAndMarkets].splice(0, 3).map(element => { return element.market }),
    chartData: [],
    difValues: []
  } as ChartsState;

  chp = this.changePair.bind(this);
  chm = this.changeMarkets.bind(this);
  gs = this.getSignals.bind(this);
  as = this.addSignal.bind(this);
  us = this.updateSignals.bind(this);

  ws: WebSocket = new WebSocket(process.env.REACT_APP_WS as string);

  async setPair(): Promise<string> {
    return new Promise(async resolve => {
      const pairs = await this.parsePairs();
      resolve(pairs[0]);
    });
  }

  async parsePairs(): Promise<string[]> {
    return new Promise(async resolve => {
      const data: PairName[] = pairsNames as PairName[];
      let pairs: string[] = [];
      for await (const entry of data) {
        pairs.push(entry.destination);
      }
      resolve(pairs);
    });
  }

  getSignals() {
    const cookies: Cookies = new Cookies();
    const token: string = cookies.get('token');
    if (token && this.state.markets.length > 0) {
      const headers: Object = { headers: { "Authorization": `Bearer ${token}` } }
      axios.get(`${process.env.REACT_APP_API}/signals?pair=${this.state.pair}&markets=${this.state.markets.join(',')}`, headers)
        .then((response: AxiosResponse) => {
          const data: SpecificSignalsResponse = response.data as SpecificSignalsResponse;
          if (data.code === 'Success') {
            this.setState({ difValues: data.message });
          } else {
            console.error(data.message);
          }
        })
        .catch((error: AxiosError) => {
          if (error.response === undefined) {
            toast.error(`Can't receive Signals data`, {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true });
          } else {
            const data: CommonResponse = error.response.data as CommonResponse;
            toast.error(data.message, {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true });
            if (data.message === 'Authorization token is missing' || data.message === 'Wrong token' || data.message === 'Invalid token') {
              this.props.logOut();
            }
          }
        })
    } else {
      this.props.logOut();
    }
  }

  addSignal(signal: string) {
    const cookies: Cookies = new Cookies();
    const token: string = cookies.get('token');
    if (token) {
      const headers: Object = { headers: { "Authorization": `Bearer ${token}` } }
      const body: Object = { "pair": this.state.pair, "markets": this.state.markets, "difference": +signal }
      axios.post(`${process.env.REACT_APP_API}/signal`, body, headers)
        .then((response: AxiosResponse) => {
          const data: SpecificSignalsResponse = response.data as SpecificSignalsResponse;
          if (data.code === 'Success') {
            let values: DifValue[] = this.state.difValues as DifValue[];
            if (!values) {
              values = [];
            }
            values.push({
              value: +signal,
              label: +signal,
              sid: +data.message
            });
            this.setState({ difValues: values });
          } else {
            toast.error(data.message, {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true });
          }
        })
        .catch((error: AxiosError) => {
          if (error.response === undefined) {
            toast.error(`Can't save Signal`, {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true });
          } else {
            const data: CommonResponse = error.response.data as CommonResponse;
            if (data.code === 'Error' && data.message === 'Wrong token') {
              this.props.logOut();
              toast.error(data.message, {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true });
            } else if (data.code === 'Error' && data.message === 'Too many signals for single user') {
              toast.info(data.message, {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true });
            } else {
              toast.error(data.message, {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true });
            }
          }
        });
    }
  }

  async updateSignals(newSignals: ValueType<DifValue>): Promise<void> {
    return new Promise(resolve => {
      this.setState({ difValues: newSignals });
      resolve();
    });
  }

  async changePair(pair: SelectOption): Promise<void> {
    return new Promise(async resolve => {
      await new Promise(resolve => {
        this.setState({ pair: pair.value });
        resolve();
      })
      await this.sendMessage();
      if (this.props.loggedIn) {
        this.getSignals();
      }
      resolve();
    });
  }

  async nullMarkets(): Promise<void> {
    return new Promise(resolve => {
      this.setState({
        chartData: [],
        markets: []
      });
      resolve();
    });
  }

  async changeMarkets(markets: SelectOption[]): Promise<void> {
    return new Promise(async resolve => {
      if (!markets || markets.length === 0) {
        await this.nullMarkets();
      } else {
        let marketsToSend: string[] = [];
        for await (let market of markets) {
          marketsToSend.push(market.value);
        }
        this.setState({
          markets: marketsToSend
        });
      }
      await this.sendMessage();
      if (this.props.loggedIn && this.state.markets.length > 0) {
        this.getSignals();
      }
      resolve();    
    });
  }

  async sendMessage(): Promise<void> {
    return new Promise(resolve => {
      const mData: MarketData = {
        pair: this.state.pair,
        markets: this.state.markets
      }
      this.ws.send(JSON.stringify(mData));
      resolve();
    });
  }

  componentDidMount() {
    this.props.setActiveTab('Charts');
    this.ws.onopen = () => {
      const mData: MarketData = {
        pair: this.state.pair,
        markets: this.state.markets
      } as MarketData;
      this.ws.send(JSON.stringify(mData));
    }
    this.ws.onmessage = (msg: MessageEvent) => {
      const data: DataEntry[] = JSON.parse(msg.data) as DataEntry[];
      this.setState({
        chartData: data
      });
    }
    this.ws.onclose = function(msg: CloseEvent) {
      console.warn(`WebSocket disconnected, reason code: '${msg.code}', reason message: '${msg.reason}'.`);
    }
    this.ws.onerror = function(err: Event) {
      console.error(`WebSocket error: '${err}'.`);
    }
  }

  componentWillUnmount() {
    this.ws.close();
  }

  render() {
    return(
      <div>
        { this.state.markets.length === 0 ?
          null :
          <Chart pair={ this.state.pair } chartData={ this.state.chartData } />
        }
        <Options 
          pair={ this.state.pair } 
          markets={ this.state.markets } 
          changePair={ this.chp } 
          changeMarkets={ this.chm } 
        />
        { 
          (this.props.loggedIn && this.state.markets.length >= 2 ) ? 
            <Signals 
              markets={ this.state.markets }
              difValues={ this.state.difValues } 
              getSignals={ this.gs } 
              addSignal={ this.as } 
              deleteSignal={ this.props.deleteSignal } 
              updateSignals={ this.us } 
            /> : null 
        }
      </div>
    );
  }
}

export default Charts;