import React, { Component } from 'react';
import { MDBContainer, MDBCard, MDBCardHeader, MDBCardBody, MDBCardText } from 'mdbreact';

class About extends Component<AboutProps> {
  componentDidMount() {
    this.props.setActiveTab('About');
  }

  render() {
    return(
      <MDBContainer>
        <MDBCard border="light" className="m-3">
          <MDBCardHeader>Information</MDBCardHeader>
          <MDBCardBody>
            <MDBCardText>
              This is second version of Cryptocurrency Arbitrage project. 
              You may find screenshots of previous version on my personal website (as well as contacts). 
              <br />The concept of this project was created by me when I was making my graduate work for bachelor's degree. 
              <br />Source code of this project may be found on GitLab.
            </MDBCardText>
          </MDBCardBody>
        </MDBCard>
        <MDBCard border="light" className="m-3">
          <MDBCardHeader>Contacts</MDBCardHeader>
          <MDBCardBody>
            <MDBCardText>
              <a href="https://www.autoqa.me" target="_blank" rel="noopener noreferrer">My landing page (resume, portfolio, personal contacts)</a>
              <br /><a href="https://gitlab.com/carbitrage" target="_blank" rel="noopener noreferrer">Source code of this project on GitLab</a>
            </MDBCardText>
          </MDBCardBody>
        </MDBCard>
        <MDBCard border="light" className="m-3">
          <MDBCardHeader>Donations</MDBCardHeader>
          <MDBCardBody>
            <MDBCardText>
              <b>Bitcoin</b>: 153Jkiz52QkCxhevZt3Durxh5sd1CWKxSu
              <br /><b>Ethereum</b>: 0x73f245a066661bd6ea8514bbaed4e8bf49c7081b 
              <br /><b>Yandex.Money</b>: 410011962256485
            </MDBCardText>
          </MDBCardBody>
        </MDBCard>
      </MDBContainer>
    );   
  }
}

export default About;