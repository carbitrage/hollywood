import React, { Component } from 'react';
import Cookies from 'universal-cookie';
import { toast } from 'react-toastify';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
import 'react-toastify/dist/ReactToastify.css';
import Navbar from './components/navbar/Navbar';
import Charts from './pages/charts/Charts';
import Auth from './components/auth/Auth';
import Home from './pages/home/Home';
import About from './pages/about/About';
import Profile from './pages/profile/Profile';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import axios from 'axios';
import './App.css';
import Lost from './pages/lost/Lost';

class App extends Component {
  state: AppState = {
    tab: '',
    navbarIsOpen: false,
    authModalIsOpen: false,
    loggedIn: this.isLoggedIn(),
    token: '',
    width: window.innerWidth,
    height: window.innerHeight
  } as AppState;

  tgc = this.toggleCollapse.bind(this);
  tgam = this.toggleAuthModal.bind(this);
  li = this.logIn.bind(this);
  lo = this.logOut.bind(this);
  sat = this.setActiveTab.bind(this);
  uwd = this.updateWindowDimensions.bind(this);
  ds = this.deleteSignal.bind(this);

  isLoggedIn(): boolean {
    const cookies: Cookies = new Cookies();
    const token: string = cookies.get('token');
    if (token) { return true } 
    else { return false }
  }

  setActiveTab(activeTab: string) {
    this.setState({ tab: activeTab });
  }

  logIn(tokenToWrite: string) {
    const cookies: Cookies = new Cookies();
    if (process.env.REACT_APP_DOMAIN) {
      cookies.set('token', tokenToWrite, { path: '/', domain: process.env.REACT_APP_DOMAIN });
    } else {
      cookies.set('token', tokenToWrite, { path: '/' });
    }
    this.setState({ loggedIn: true, token: tokenToWrite });
  }

  logOut() {
    this.setState({ loggedIn: false });
    const cookies: Cookies = new Cookies();
    if (process.env.REACT_APP_DOMAIN) {
      cookies.remove('token', { path: '/', domain: process.env.REACT_APP_DOMAIN });
    } else {
      cookies.remove('token', { path: '/' });
    }
  }

  toggleCollapse() {
    this.setState({ navbarIsOpen: !this.state.navbarIsOpen });
  }

  toggleAuthModal() {
    this.setState({ authModalIsOpen: !this.state.authModalIsOpen });
  }

  async deleteSignal(sid: number): Promise<boolean> {
    return new Promise(resolve => {
      const cookies: Cookies = new Cookies();
      const token: string = cookies.get('token');
      if (token) {
        const headers: Object = { headers: { "Authorization": `Bearer ${token}` }, data: { "sid": sid } }
        axios.delete(`${process.env.REACT_APP_API}/signal`, headers)
          .then(response => {
            const data: SpecificSignalsResponse = response.data as SpecificSignalsResponse;
            if (data.code === 'Success') {
              resolve(true);
            } else {
              toast.error(data.message, {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true });
              resolve(false);
            }
          })
      } else {
        this.logOut();
        resolve(false);
      }
    });
  }

  componentDidMount() {
    window.addEventListener('resize', this.uwd);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.uwd);
  }

  updateWindowDimensions() {
    this.setState({ width: window.innerWidth, height: window.innerHeight });
  }

  render() {
    return(
      <Router>
        <Navbar 
          isOpen={ this.state.navbarIsOpen } 
          loggedIn={ this.state.loggedIn } 
          toggleCollapse={ this.tgc } 
          toggleAuthModal={ this.tgam } 
          logOut={ this.lo } 
          tab={ this.state.tab } 
          setActiveTab={ this.sat }
        />
        <Auth 
          isOpen={ this.state.authModalIsOpen } 
          toggleAuthModal={ this.tgam } 
          logIn={ this.li } 
        />
        <ToastContainer
          position="top-right"
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
        />
        <Switch>
          <Route exact path="/">
            <Home width={ this.state.width } loggedIn={ this.state.loggedIn } setActiveTab={ this.sat } />
          </Route>
          <Route exact path="/charts">
            <Charts loggedIn={ this.state.loggedIn } logOut={ this.lo } deleteSignal={ this.ds } setActiveTab={ this.sat } />
          </Route>
          <Route exact path="/about">
            <About setActiveTab={ this.sat } />
          </Route>
          <Route exact path="/profile" >
            { this.state.loggedIn ? 
              <Profile deleteSignal={ this.ds } setActiveTab={ this.sat } logOut={ this.lo } /> :
              <Redirect to='/' /> }
          </Route>
          <Route>
            <Lost setActiveTab={ this.sat } />
          </Route>
        </Switch>
      </Router>
    );
  }
}

export default App;
