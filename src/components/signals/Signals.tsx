import React, { Component } from 'react';
import { MDBContainer, MDBCard, MDBCardHeader, MDBCardBody, MDBRow, MDBCol } from 'mdbreact';
import Select from 'react-select/creatable';
import { ValueType } from 'react-select';

class Signals extends Component<SignalsProps> {
  state: SignalsState = {
    signalsInput: ''
  } as SignalsState;

  componentDidMount() {
    if (this.props.markets.length > 0) {
      this.props.getSignals();
    }
  }

  onSignalsInputChange(inputValue: string) {
    if (!isNaN(+inputValue) && inputValue.length < 9 && inputValue !== undefined) {
      this.setState({
        signalsInput: inputValue
      });
    }
  }

  async onSignalsValuesChange(modifiedValues: ValueType<DifValue>) {
    let valueToChange: ValueType<DifValue>;
    let existedValues: ValueType<DifValue> = this.props.difValues;
    if (modifiedValues === null) {
      if (existedValues.length > 0) {
        valueToChange = existedValues[0];
        this.props.deleteSignal(valueToChange.sid);
        this.props.updateSignals(modifiedValues);
      }
    } else {
      let stringsBefore: string[] = [];
      let stringsAfter: string[] = [];
      if (!existedValues) {
        existedValues = [];
      }
      for await (const value of existedValues) {
        stringsBefore.push(JSON.stringify(value));
      }
      for await (const value of modifiedValues!) {
        stringsAfter.push(JSON.stringify(value));
      }
      if (stringsAfter.length < stringsBefore.length) {
        for await (const value of stringsBefore) {
          if (!stringsAfter.includes(value)) {
            valueToChange = JSON.parse(value) as DifValue;
            this.props.deleteSignal(valueToChange.sid);
            this.props.updateSignals(modifiedValues);
          }
        }
      } else {
        for await (const value of stringsAfter) {
          if (!stringsBefore.includes(value)) {
            valueToChange = JSON.parse(value) as DifValue;
            const valueToAdd = valueToChange.value.toString();
            this.props.addSignal(valueToAdd);
          }
        }
      }
    }
  }

  osic = this.onSignalsInputChange.bind(this);
  osvc = this.onSignalsValuesChange.bind(this);

  render() {
    return(
      <MDBContainer>
        <MDBCard border="light" className="m-3">
          <MDBCardHeader>Signals</MDBCardHeader>
          <MDBCardBody>
            <MDBContainer>
              <MDBRow>
                <MDBCol style={{ marginBottom: '.75rem' }}>
                  <Select
                    options={ [] }
                    placeholder="Insert signals here"
                    isMulti
                    value={ this.props.difValues }
                    inputValue={ this.state.signalsInput }
                    onInputChange={ inputValue => { this.osic(inputValue) } }
                    onChange={ changedValue => { this.osvc(changedValue) } }
                    isDisabled={ this.props.markets.length === 0 }
                  />
                </MDBCol>
              </MDBRow>
            </MDBContainer>
          </MDBCardBody>
        </MDBCard>
      </MDBContainer>
    );
  }
}

export default Signals;