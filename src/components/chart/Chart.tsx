import React, { Component } from "react";
import { ResponsivePie } from '@nivo/pie';
import { MDBCard, MDBCardBody, MDBCardHeader, MDBContainer } from "mdbreact";

class Chart extends Component<ChartProps> {
  render() {
    return (
      <MDBContainer>
        <MDBCard border="light" className="m-3">
          <MDBCardHeader>{ this.props.pair }</MDBCardHeader>
          <MDBCardBody style={ this.props.chartData.length === 0 ? { } : { height: 400 }}>
            {
              this.props.chartData.length === 0 ?
              <div style={ { display: 'flex', justifyContent: 'center' } }>
                <div className="spinner-border" role="status">
                  <span className="sr-only">Loading...</span>
                </div>
              </div> :
              <ResponsivePie
                data={ this.props.chartData }
                margin={{ top: 0, right: 60, bottom: 80, left: 60 }}
                innerRadius={0.5}
                padAngle={0.7}
                cornerRadius={3}
                colors={{ scheme: 'nivo' }}
                borderWidth={1}
                borderColor={{ from: 'color', modifiers: [ [ 'darker', 0.2 ] ] }}
                radialLabelsSkipAngle={10}
                radialLabelsTextXOffset={4}
                radialLabelsTextColor="#333333"
                radialLabelsLinkOffset={0}
                radialLabelsLinkDiagonalLength={12}
                radialLabelsLinkHorizontalLength={12}
                radialLabelsLinkStrokeWidth={1}
                radialLabelsLinkColor={{ from: 'color' }}
                slicesLabelsSkipAngle={10}
                slicesLabelsTextColor="#333333"
                animate={true}
                motionStiffness={90}
                motionDamping={15}
                defs={[
                  {
                    id: 'dots',
                    type: 'patternDots',
                    background: 'inherit',
                    color: 'rgba(255, 255, 255, 0.3)',
                    size: 4,
                    padding: 1,
                    stagger: true
                  },
                  {
                    id: 'lines',
                    type: 'patternLines',
                    background: 'inherit',
                    color: 'rgba(255, 255, 255, 0.3)',
                    rotation: -45,
                    lineWidth: 6,
                    spacing: 10
                  }
                ]}
                legends={[
                  {
                    anchor: 'bottom',
                    direction: 'row',
                    translateY: 56,
                    itemWidth: 80,
                    itemHeight: 18,
                    itemTextColor: '#999',
                    symbolSize: 18,
                    symbolShape: 'circle',
                    effects: [
                      {
                        on: 'hover',
                        style: {
                            itemTextColor: '#000'
                        }
                      }
                    ]
                  }
                ]}
              />
            }
          </MDBCardBody>
        </MDBCard>
      </MDBContainer>
    );
  }
}

export default Chart;