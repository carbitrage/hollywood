import React, { Component } from "react";
import axios, { AxiosResponse, AxiosError } from 'axios';
import 'dotenv';
const { MDBContainer, MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter } = require('mdbreact');

const initialState: AuthState = {
  uid: '',
  password: '',
  sent: false,
  errMsg: '',
  errCode: 0, // 0 - no error, 1 - uid < 8, 2 - wrong id, 3 - password < 4, 4 - wrong password
  uidInputStyle: { },
  passwordInputStyle: { },
  requestProcessing: false
} as AuthState;

class Auth extends Component<AuthOpts> {
  state: AuthState = initialState as AuthState;

  sp = this.sendPassword.bind(this);
  ouidc = this.onUserIDChange.bind(this);
  cuidv = this.checkUserIDValidity.bind(this);
  opc = this.onPasswordChange.bind(this);
  cpv = this.checkPasswordValidity.bind(this);
  rs = this.reSend.bind(this);
  li = this.logIn.bind(this);

  logIn() {
    this.setState({ requestProcessing: true });
    const getToken: AuthRequest = { uid: +this.state.uid, password: this.state.password } as AuthRequest;
    axios.post(`${process.env.REACT_APP_API}/auth`, getToken)
      .then((response: AxiosResponse) => {
        const data: CommonResponse = response.data as CommonResponse;
        if (data.code === 'Success') {
          this.props.logIn(data.message);
          this.props.toggleAuthModal();
          this.setState(initialState);
        }
        else {
          this.setState({ errCode: 4, errMsg: data.message, passwordInputStyle: { borderColor: 'red' } });
        }
        this.setState({ requestProcessing: false });
      })
      .catch((error: AxiosError) => {
        if (error.response === undefined) {
          this.setState({ errCode: 4, errMsg: 'Unknown authentication error', passwordInputStyle: { borderColor: 'red' } });
        } else {
          const data: CommonResponse = error.response.data as CommonResponse;
          if (data.code === 'Error') {
            this.setState({ errCode: 4, errMsg: data.message, passwordInputStyle: { borderColor: 'red' } });
          }
        }
        this.setState({ requestProcessing: false });
      });
  }

  sendPassword() {
    this.setState({ requestProcessing: true });
    const getPass: PasswordRequest = { uid: +this.state.uid } as PasswordRequest;
    axios.post(`${process.env.REACT_APP_API}/getpass`, getPass)
      .then((response: AxiosResponse) => {
        const data: CommonResponse = response.data as CommonResponse;
        if (data.code === 'Success') {
          this.setState({ sent: true });
        }
        else {
          this.setState({ errCode: 2, errMsg: data.message, uidInputStyle: { borderColor: 'red' } });
        }
        this.setState({ requestProcessing: false });
      })
      .catch((error: AxiosError) => {
        if (error.response === undefined) {
          this.setState({ errCode: 2, errMsg: 'Unknown send password error', uidInputStyle: { borderColor: 'red' } });
        } else {
          const data: CommonResponse = error.response.data as CommonResponse;
          if (data.code === 'Error') {
            this.setState({ errCode: 2, errMsg: data.message, uidInputStyle: { borderColor: 'red' } });
          }
        }
        this.setState({ requestProcessing: false });
      })
  }

  reSend() {
    this.setState({
      sent: false,
      password: '',
      errCode: 0,
      errMsg: '',
      uidInputStyle: { },
      passwordInputStyle: { }
    });
  }

  onUserIDChange(e: any) {
    if (!e.target.validity.badInput) {
      const value: string = e.target.value.replace('.', '');
      if (!isNaN(+value) && !value.includes('.')) {
        if (value.length > 12) {
          this.setState({uid: value.slice(0, 12)});
        }
        else {
          this.setState({uid: value})
        }
        if (this.state.errCode === 1 || this.state.errCode === 2) {
          this.setState({ errCode: 0, errMsg: '', uidInputStyle: { } });
        }
      }
    }
  }

  checkUserIDValidity() {
    if (this.state.uid.length < 1 || this.state.uid.length > 12) {
      this.setState({ errCode: 1, errMsg: 'User ID should contain 1-12 digits', uidInputStyle: { borderColor: 'red' } });
    }
  }

  onPasswordChange(event: React.ChangeEvent<HTMLInputElement>) {
    if (!event.target.validity.badInput) {
      let value: string = event.target.value.replace('.', '');
      if (!isNaN(+value) && !value.includes('.')) {
        if (value.length > 4) {
          this.setState({password: value.slice(0, 4)});
        }
        else {
          this.setState({password: value})
        }
        if (this.state.errCode === 3 || this.state.errCode === 4) {
          this.setState({
            errCode: 0, 
            errMsg: '', 
            passwordInputStyle: { } 
          });
        }
      }
    }
  }

  checkPasswordValidity() {
    if (this.state.password.length < 4) {
      this.setState({
        errCode: 3, 
        errMsg: 'Password should contain 4 digits', 
        passwordInputStyle: { borderColor: 'red' } 
      });
    }
  }

  render() {
    let errorMessage: JSX.Element | null;
    let inputsgroup: JSX.Element | null;
    let btngroup: JSX.Element;
    if (this.state.errCode !== 0) {
      errorMessage = 
        <span style={{ display: 'block', textAlign: 'center', marginBottom: '0.5rem', color: 'red' }} data-testid="errorMessage">{ this.state.errMsg }</span>;
    }
    else {
      errorMessage = null;
    }
    if (this.state.sent === false) {
      inputsgroup =
        <MDBModalBody>
          <div className="input-group" style={{ marginBottom: "1em" }}>
            <div className="input-group-prepend">
              <span style={ this.state.uidInputStyle } className="input-group-text" id="basic-addon">
                <i className="fa fa-user prefix"></i>
              </span>
            </div>
            <input 
              style={ this.state.uidInputStyle } 
              type="text" 
              pattern="[0-9]" 
              className="form-control" 
              placeholder="User ID" 
              aria-label="Username" 
              aria-describedby="basic-addon" 
              disabled={ this.state.sent || this.state.requestProcessing } 
              value={ this.state.uid } 
              onChange={ (event: React.ChangeEvent<HTMLInputElement>) => this.ouidc(event) } 
              onBlur={ () => this.cuidv() }
            />
          </div>
          { errorMessage }
          <span style={{ textAlign: "center", display: "block" }}>Send <b>'/start'</b> message to <a href="https://t.me/carbi_smart_bot" target="_blank" rel="noopener noreferrer">our Telegram bot</a> to get your User ID</span>
        </MDBModalBody>
    }
    else {
      inputsgroup = 
        <MDBModalBody>
          <div className="input-group" style={{ marginBottom: "1em" }}>
            <div className="input-group-prepend">
              <span style={ this.state.uidInputStyle } className="input-group-text" id="basic-addon">
                <i className="fa fa-user prefix"></i>
              </span>
            </div>
            <input 
              style={ this.state.uidInputStyle } 
              type="text" 
              pattern="[0-9]" 
              className="form-control" 
              placeholder="User ID" 
              aria-label="Username" 
              aria-describedby="basic-addon" 
              disabled={ this.state.sent || this.state.requestProcessing } 
              value={ this.state.uid } 
              onChange={ (event: React.ChangeEvent<HTMLInputElement>) => this.ouidc(event) } 
              onBlur={ () => this.cuidv() }
            />
          </div>
          <div className="input-group" style={{ marginBottom: "1em" }}>
            <div className="input-group-prepend">
              <span style={ this.state.passwordInputStyle } className="input-group-text" id="basic-addon">
                <i className="fa fa-lock prefix"></i>
              </span>
            </div>
            <input 
              style={ this.state.passwordInputStyle } 
              type="password" 
              pattern="[0-9]" 
              className="form-control" 
              placeholder="Password" 
              aria-label="Password" 
              aria-describedby="basic-addon" 
              value={ this.state.password } 
              onChange={ (event: React.ChangeEvent<HTMLInputElement>) => this.opc(event) } 
              disabled={ this.state.requestProcessing }
              onBlur={ () => this.cpv() }
            />
          </div>
          { errorMessage }
          <span style={{ textAlign: "center", display: "block" }}>Send <b>'/start'</b> message to <a href="https://t.me/carbi_smart_bot" target="_blank" rel="noopener noreferrer">our Telegram bot</a> to get your User ID</span>
        </MDBModalBody>;
    }
    if (this.state.sent === false) {
      btngroup =
        <MDBModalFooter>
          <MDBBtn 
            color="primary" 
            onClick={ this.sp } 
            disabled={ ((this.state.errCode === 0 && this.state.uid.length > 1 && this.state.uid.length < 13) ? false : true) || this.state.requestProcessing }
          >
            Send Password
          </MDBBtn>
        </MDBModalFooter>
    }
    else {
      btngroup =
        <MDBModalFooter>
          <MDBBtn 
            color="secondary" 
            onClick={ this.rs } 
            disabled={ this.state.requestProcessing }
          >
            Re-Send
          </MDBBtn>
          <MDBBtn 
            color="primary" 
            onClick={ this.li } 
            disabled={ ((this.state.errCode === 0 && this.state.password.length === 4) ? false : true) || this.state.requestProcessing }
          >
            Log In
          </MDBBtn>
        </MDBModalFooter>
    }
    return(
      <MDBContainer>
        <MDBModal isOpen={ this.props.isOpen } toggle={ this.props.toggleAuthModal } centered>
          <MDBModalHeader toggle={ this.props.toggleAuthModal }>Log In</MDBModalHeader>
            { inputsgroup }
            { btngroup }
        </MDBModal>
      </MDBContainer>
    );
  }
}

export default Auth;