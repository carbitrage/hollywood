import React, { Component } from 'react';
import { MDBContainer, MDBCard, MDBCardHeader, MDBCardBody, MDBCardTitle, MDBRow, MDBCol } from 'mdbreact';
import Select, { ValueType } from 'react-select';
import pairsAndMarkets from '../../data/markets.json';
import pairsNames from '../../data/pairs.json';
import './Options.css';

class Options extends Component<OptionsProps> {
  render() {
    const pairs: SelectOption[] = [...pairsNames].map(element => { 
      return { value: element.destination, label: element.destination } as SelectOption
    });
    const markets: SelectOption[] = [...pairsAndMarkets].map(element => {
      return { value: element.market, label: element.market } as SelectOption
    });
    let defaultPair: SelectOption = { value: this.props.pair, label: this.props.pair } as SelectOption;
    let defaultMarkets: SelectOption[] = this.props.markets.map(element => { 
      return { value: element, label: element } as SelectOption 
    });
    return(
      <MDBContainer>
        <MDBCard border="light" className="m-3">
          <MDBCardHeader>Options</MDBCardHeader>
          <MDBCardBody>
            <MDBContainer>
              <MDBRow>
                <MDBCol lg="6" style={{ marginBottom: '.75rem' }}>
                  <MDBCardTitle tag="h5" className="text-center">Pair</MDBCardTitle>
                  <Select
                    options={ pairs }
                    placeholder="Pair"
                    defaultValue={ defaultPair }
                    onChange={ (selectedOption: ValueType<SelectOption>) => { this.props.changePair(selectedOption as SelectOption) } }
                  />
                </MDBCol>
                <MDBCol lg="6">
                  <MDBCardTitle tag="h5" className="text-center">Markets</MDBCardTitle>
                  <Select
                    placeholder="Markets"
                    options={ markets }
                    defaultValue={ defaultMarkets }
                    isMulti
                    name="colors"
                    className="basic-multi-select"
                    classNamePrefix="select"
                    onChange={ (selectedOption: ValueType<SelectOption>) => { this.props.changeMarkets(selectedOption as SelectOption[]) } }
                  />
                </MDBCol>
              </MDBRow>
            </MDBContainer>
          </MDBCardBody>
        </MDBCard>
      </MDBContainer>
    );
  }
}

export default Options;